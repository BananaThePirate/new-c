use clap::Parser;
use color_eyre::eyre::Result;
use dialoguer::Input;
use exec::execvp;
use std::{fs, path::Path};

#[derive(Parser, Debug)]
struct Args {
    /// Name of the project
    name: String,
}

fn main() -> Result<()> {
    color_eyre::install()?;
    let args = Args::parse();
    let dir = Path::new(&args.name);
    let src_dir = dir.join("src/");
    fs::create_dir(dir).expect("Unable to create directory");
    fs::create_dir(&src_dir).expect("Unable to create src directory");
    let name: String = Input::new()
        .with_prompt("name of the binary")
        .interact_text()
        .expect("unable to read input");
    let make_str = format!(include_str!("files/Makefile"), &name, &name);
    fs::write(dir.join("Makefile"), make_str).expect("unable to write Makefile");
    let join = src_dir.join("main.c");
    let main_name = join.to_str().expect("Strings are hard");
    let err = execvp("nf", &["nf", main_name]);
    Err(err.into())
}
